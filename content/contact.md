---
url: "/contact"
draft: false
---

### Contacts and Community

[Telegram Announcements Channel](https://t.me/GhostchainAnnouncements)

[Tezos Dev Slack](https://tezos-dev.slack.com/archives/C02RQ0LHQLA)

[Discord Server](https://discord.gg/W3WdntUxTf)

[Email](ghostchain.net@gmail.com) 


