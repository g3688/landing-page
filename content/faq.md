# Ghostchain FAQ

### How much does Ghostchain cost? 

Ghostchain is entirely free.

### Whom is Ghostchain for?

Ghostchain is meant for dapp developers and will provide some specific tooling to help them test dapp deployed on this chain on future protocols as well as daily builds of the alpha protocol.

### Whom is Ghostchain not for?

To ensure predictable performance and block times, block producers on Ghostchain are heavily centralized, this means that Ghostchain is not
suitable for users seeking to test a baker setup.

### How is Ghostchain funded? 

Ghostchain is developed and funded by volunteers and donations from community memebers.
