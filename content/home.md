---
url: "/about"
draft: false
---


# Ghostchain

Is a testchain mirroring the Tezos network. It is built for the dapp development partners in the Tezos ecosystem. "Mirroring" Tezos, means that it will keep the same protocol as Tezos and upgrade to whatever protocol the Tezos community votes on. Ghostchain is centralized with 90% of the tokens and baking controlled by the Ghostchain team. 

In order to provide dapp developers a stable experience and guaranteed continuous existance of the chain, the faucet will require minor registration to prevent spamming and will send proportional tokens to the central bakers so that a 90% dominance is maintained. 

If you are part of the Tezos dapp development community we would love your feedback. How can we provide you more value? Please [reach out](https://tezos-dev.slack.com/archives/C02RQ0LHQLA).
 




